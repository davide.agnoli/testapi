import org.http4k.core.*
import org.http4k.core.Status.Companion.OK
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.Netty
import org.http4k.server.asServer
import org.http4k.format.Jackson.auto


fun main() {
    val port = 1313
    createApp()
            .asServer(Netty(port))
            .start()

    println("App running on http://localhost:$port")
}

fun createApp() = routes(
        "person-details" bind Method.GET to ::getPerson,
        "greeting" bind Method.GET to ::getGreeting,
)


fun getPerson(request: Request): Response {
    val name: String? = request.query(name = "name")
    val person = PersonDetails(name ?: "Davide")
    val responseShape = Body.auto<PersonDetails>().toLens()
    return Response(OK).with(
            responseShape of person
    )

}

fun getGreeting(request: Request): Response {
    val name: String? = request.query(name = "name")
    val person = PersonDetails(name ?: "Davide")
    var uriGreeting = "http://localhost:9000/get-greeting?name=${person.name}"
    println("urigreeting= $uriGreeting")
    val responseFromGreetingServer = Request(Method.GET, "http://localhost:9000/get-greeting?name=Da").bodyString()
    println ("responseFromGreetingServer= $responseFromGreetingServer")
    val result = Response(OK).body(responseFromGreetingServer)
    println("result= $result")
    return result

}
data class PersonDetails(
        val name: String
)