import org.http4k.core.Method
import org.http4k.core.Request
import org.junit.jupiter.api.Test
import org.assertj.core.api.Assertions.assertThat
import org.http4k.core.Status

internal class ApplicationKtTest {

    @Test
    fun `person-details with no name specified should default to Davide`() {
        // Given no query parameter passed

        // When
        val result = createApp()(Request(Method.GET, "/person-details"))

        // Then name is defaulted to Davide
        assertThat(result.status).isEqualTo(Status.OK)
        assertThat(result.bodyString()).isEqualTo("""{"name":"Davide"}""")
    }

    @Test
    fun `person-details should use name if specified`() {
        // Given a name query parameter is passed

        // When
        val result = createApp()(Request(Method.GET, "/person-details?name=Carolyn"))

        // Then name is NOT defaulted to Davide
        assertThat(result.status).isEqualTo(Status.OK)
        assertThat(result.bodyString()).isEqualTo("""{"name":"Carolyn"}""")
    }

    @Test
    fun `with no name specified should default to Davide`() {
        // Given a name query parameter is passed

        // When
        val result = createApp()(Request(Method.GET, "/greeting"))

        // Then name is defaulted to Davide
        assertThat(result.status).isEqualTo(Status.OK)
        assertThat(result.bodyString()).isEqualTo("Hello Davide")
    }
    @Test
    fun `greeting should use name if specified`() {
        // Given a name query parameter is passed

        // When
        val result = createApp()(Request(Method.GET, "/greeting?name=Carolyn"))

        // Then name is defaulted NOT to Davide
        assertThat(result.status).isEqualTo(Status.OK)
        assertThat(result.bodyString()).isEqualTo("Hello Carolyn")
    }


}